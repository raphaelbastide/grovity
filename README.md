# Grovity

Sequencer for rhythmic compositions with a focus on micro-rhythms.

Main features of Grovity:

- Time shift of each pulse allowing a precise tuning of swings, grooves and other temporal dynamics
- GPIO output: Grovity can send signals to motors (solenoids) interfaced on a Raspberry pi
- MIDI output
- Ability to compose polyrhythms
- Generated euclidean rhythmic patterns
- Bright / Dark mode

Made with Vue.js, Tone.js and WebSocket.

![Screenshot](screenshot.png)

Short video demos: [#grovity](https://peertube.social/search?tagsOneOf=grovity) on my peertube channel.

## Build Setup

  npm install

## serve with hot reload at localhost:8080

  npm run dev

## build for production with minification

  npm run build

## Testing

Rename `config/default_index.js` to `config/index.js`.

You can test on a computer withou GPIO, like a normal computer that is not a raspberry pi, by changing the value of `hasGPIO` in `config/index.js`.

## Inspirations

Thanks to Benjamin Steephenson and his great Step-Sequencer https://github.com/bsteephenson/Step-Sequencer

## Todo

- Negative offset 🤔
- `Socket.io` stuck to `2.2.0` because of [this issue](https://github.com/socketio/socket.io-client/issues/1328) so I need to keep an eye on it to make an upgrade possible

## Hardware

I use
- a Raspberry Pi 3 model B
- 3+ solenoids: [DC6V DC12V DC24V HCNE1-0530 Open frame type electromagnet push-pull type 5N 10mm tensile](https://www.aliexpress.com/item/32810049065.html)
- 1 solenoid 25X50TL [Push-pull type crash type miniature electromagnet DC 12V 24V Tubular Electric Solenoid Electromagnet Stroke 10-15mm 300g](https://www.aliexpress.com/item/32812889837.html)
- For each solenoid used, 1 [IRF520 MOSFET Driver Module](https://www.aliexpress.com/item/32810805866.html)

## Other prototypes

See independent README files in `prototypes/`.
