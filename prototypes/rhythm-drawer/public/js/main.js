var app = new Vue({
  el: '#app',
  data: {
    title: "Rhythm Drawer",
    boxes: {
      matrix :16,
    },
    options: [
      { text: '4', value: '4' },
      { text: '9', value: '9' },
      { text: '16', value: '16' },
      { text: '25', value: '25' },
      { text: '36', value: '36' }
    ],
    checkedBoxes: [],
    pattern:[],
  },
  mounted() {
    this.generatePattern()
  },
  methods: {
    generatePattern: function(){
      this.pattern = []
      for (var i = 0; i < this.boxes.matrix * this.boxes.matrix; i++) {
        this.pattern.push(0)
        // this.pattern = toString(this.pattern)
      }
    },
    updatePattern: function (n){
      isChecked = this.checkedBoxes.includes(n)
      this.pattern[n-1] = +isChecked // + forces boolean to be 1 and 0
    },
  },
  props: {
  },
})
