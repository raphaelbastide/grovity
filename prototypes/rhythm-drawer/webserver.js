var test = true

var http = require('http').createServer(handler); //require http server, and create server with function handler()
var fs = require('fs'); //require filesystem module
var io = require('socket.io')(http) //require socket.io module and pass the http object (server)

if (!test) {
  var Gpio = require('onoff').Gpio; //include onoff to interact with the GPIO
  var LED = new Gpio(4, 'out'); //use GPIO pin 4 as output
}

if (!test) {
  var pushButton = new Gpio(17, 'in', 'both'); //use GPIO pin 17 as input, and 'both' button presses, and releases should be handled
}
var port = 8080
//http.listen(port); //listen to port
var ip = require("ip");
http.listen(port, () => console.log( 'In your browser: http://'+ip.address()+':'+port ))


function handler (req, res) { //create server
  fs.readFile(__dirname + '/public/index.html', function(err, data) { //read file index.html in public folder
    if (err) {
      res.writeHead(404, {'Content-Type': 'text/html'}); //display 404 on error
      return res.end("404 Not Found");
    }
    res.writeHead(200, {'Content-Type': 'text/html'}); //write HTML
    res.write(data); //write data from index.html
    return res.end();
  });
}

function hitSolenoid() { //function to start blinking
  LED.writeSync(1)
  setTimeout(function(){LED.writeSync(0)},50)
}

io.sockets.on('connection', function (socket) {// WebSocket Connection

  var lightvalue = 0; //static variable for current status
  if (!test) {
    pushButton.watch(function (err, value) { //Watch for hardware interrupts on pushButton
      if (err) { //if an error
        console.error('There was an error', err); //output error message to console
        return;
      }

      lightvalue = value;
      socket.emit('hit', lightvalue); //send button status to client
    });
  }
  socket.on('hit', function(data) { //get light switch status from client
    lightvalue = data;
    if (!test) {
      if (lightvalue != LED.readSync()) { //only change LED if status has changed
        hitSolenoid()
      }
    }
  });
});

process.on('SIGINT', function () { //on ctrl+c
  if (!test) {
    LED.writeSync(0); // Turn LED off
    LED.unexport(); // Unexport LED GPIO to free resources
  }
  pushButton.unexport(); // Unexport Button GPIO to free resources
  process.exit(); //exit completely
});
