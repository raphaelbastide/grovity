# Dolphin Speak

Image to rhytmic sequence  
Rhytmic sequence to image

![dolphinspeak screenshot](dolphinspeak.png)

## Reqirements

`sox` is reqired if you want to emit sound from your computer. Install it with `apt-install sox`.

## Usage

```
                        BPM  Pattern   Optional 2nd pattern
                          |  |         |
  $ node dolphinspeak.js 100 001100101 101011101
```

Set `hasGPIO` to `true` if you work with solenoids or other motors.

## Ressources

- https://www.w3schools.com/nodejs/nodejs_raspberrypi_gpio_intro.asp
- https://newspunch.com/scientists-reveal-that-dolphins-speak-in-a-holographic-language/

## Saved Patterns from [image2pattern](https://gitlab.com/raphaelbastide/grovity/-/tree/master/prototypes/image2pattern)

Maze
```
  0001011101110100110100010100000101000111010101000101110001010111000001010100001011101101010111100010100101000000101011000111011010100001110000101010111100010011000000000111100011111110110000011000000000011101101110101010000000001010101010101110111000101111
```
Spiral

```
  000000010001111001010000101100110101101010101101000101100111001010000010001111100
```

Composition

```
  0000000000000010000000000000001000100000011100000000001111010000000000100001000000000011101100000111000010100000010100011011110001010001000001000101000100000100010100011111110001110000000000000000000000000000000001111111100001000100000010000000011111111000
```
