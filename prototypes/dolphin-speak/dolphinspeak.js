var hasGPIO = false // true if you have motors as output
var exec = require('child_process').exec;

if (hasGPIO) {
  var Gpio = require('onoff').Gpio;
  var SOUND1 = new Gpio(4, 'out');
  var SOUND2 = new Gpio(18, 'out');
}else {
  var SOUND1 = 'play -q -n synth 0.05 sin 880';
  var SOUND2 = 'play -q -n synth 0.05 sin 280';
}
var timers = []
var twoMotors = false
var returnInterval = 50
var bpm = 90 // default

var args = process.argv.splice(process.execArgv.length + 2)
var bpm = args[0]
var pattern1 = args[1]
if (args[2]) {
  var pattern2 = args[2]
  twoMotors = true
}

// Display the patterns
displayPattern(pattern1)
if (twoMotors) {
  displayPattern(pattern2)
}

// Play the patterns
setInterval(function(){
  timers[0] = playStep(SOUND1, pattern1, timers[0])
}, (60 / bpm) * 1000)
if (twoMotors) {
  setInterval(function(){
    timers[1] = playStep(SOUND2, pattern2, timers[1])
  }, (60 / bpm) * 1000)
}

function displayPattern(pattern){
  var matrix = Math.sqrt(pattern.length)
  if(!isInt(matrix)){
    console.log('(!) It is not a valid pattern, choose a square number of pulse')
  }else {
    console.log('matrix = '+matrix+' | bpm = '+bpm+' | cps = '+bpm / 60);
    var line = []
    for (var i = 0; i < pattern.length; i++) {
      if (pattern[i]=='1') {
        var currentChar = '●'
      }else if(pattern[i]=='0') {
        var currentChar = '·'
      }
      line.push(currentChar)
      if (line.length % matrix == 0) {
        line = line.toString()
        line = line.replace(/,/g, ' ') // remove commas
        console.log(line)
        line = []
      }
    }
  }
}

timers.push(0);
timers.push(0);

function playStep(motor, pattern, timer) {
  var note = pattern[timer]
  if (note == 1) {
    if (hasGPIO) {
      motor.writeSync(1)
      setTimeout(function(){motor.writeSync(0)}, returnInterval)
    }else {
      exec(SOUND1)
    }
  }else if(note == 0){
    if (hasGPIO) {
      motor.writeSync(0);
      setTimeout(function(){motor.writeSync(0)}, returnInterval)
    }else {
      exec(SOUND2)
    }
  }
  if (timer >= pattern.length - 1) {
    timer = 0
  }else {
    timer++
  }
  return timer
}

// Exit the program
process.stdin.resume();//so the program will not close instantly
function exitHandler(options, exitCode) {
  if (hasGPIO) {
    SOUND1.writeSync(0)
    SOUND1.unexport()
    if (twoMotors) {
      SOLENOID2.writeSync(0)
      SOUND2.unexport()
    }
  }
  if (options.cleanup) console.log('Stopped');
  if (options.exit) process.exit();
}
process.on('SIGINT', exitHandler.bind(null, {exit:true}));
//catches uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, {exit:true}));

// Functions
function isInt(value) {
  return !isNaN(value) &&
  parseInt(Number(value)) == value &&
  !isNaN(parseInt(value, 10));
}
