var app = new Vue({
  el: '#app',
  data: {
    boxes: {
      matrix :0,
    },
    elements: [],
    time:0,
    pattern:[],
    reccords:[],
    diffs:[],
    playMode: false,
    nextPulseFlag: false,
    estimTBP:0,
    timer:"",
  },
  mounted() {
    this.generatePattern()
  },
  watch:{
  },
  methods: {
    generatePattern: function(){
      this.pattern = []
      for (var i = 0; i < this.boxes.matrix * this.boxes.matrix; i++) {
        this.pattern.push(0)
      }
    },
    getPulse: function(){
      this.updateTime()
      let totalRec = this.reccords.length
      this.reccords.push(this.time)
      if (this.reccords.length <= 1) {
        // console.log('first pulse');
      }else if(!this.playMode) {
        let lastReccord = this.reccords[totalRec]
        let penulReccord = this.reccords[totalRec - 1]
        let diff = lastReccord - penulReccord
        this.diffs.push(diff)
        console.log(this.diffs);
        let sum = this.diffs.reduce((previous, current) => current += previous);
        let average = sum / this.diffs.length;
        this.estimTBP = Math.round(average)
        this.setPulse(true)
        // if (this.playMode) {
        //   this.checkedBoxes.push(this.checkedBoxes.length)
        // }
      }else{
        // this.elements.push("2")
      }

    },
    setPulse: function(manual){
      var that = this // for setTimeout
      if (manual) {
        // console.log('manual pulse');
        if (this.timer != "") {
          clearTimeout(this.timer)
        }
      }else {
        // console.log('pulse');
        this.checkPulse()
      }
      this.timer = setTimeout(function(){that.setPulse(false)},this.estimTBP)
    },
    checkPulse: function(){
      this.updateTime()
      t = this.time
      var totalReccords = this.reccords.length
      let penulReccord = this.reccords[totalReccords - 1]
      let estimNextReccord = penulReccord + this.estimTBP
      let halfEstimTBP = this.estimTBP * .5
      // locate how close is the pulse from the prev and next estimated pulse
      if (t <= penulReccord + halfEstimTBP && t >= penulReccord) {
        console.log('close to prev');
        this.nextPulseFlag = true
        // this.elements[this.elements.length] = 2
      }else if (t >= estimNextReccord - halfEstimTBP && t <= estimNextReccord) {
        console.log('close to next');
        this.elements[this.elements.length - 1] = 2
      }

      if (t >= penulReccord + this.estimTBP * 1.5 && !this.playMode) {
        this.playMode = true // go in playMode
        this.beep(240)
        this.boxes.matrix = this.reccords.length

      }
      console.log(this.playMode);
      if (this.playMode) {
        if (this.nextPulseFlag) {
          this.elements.push("2")
          this.nextPulseFlag = false
        }else {
          if (this.elements.length > this.boxes.matrix * this.boxes.matrix) {
            this.stopAll()
          }else {
            this.elements.push("1")
            this.beep(840)
          }
        }
        // this.boxes.matrix = this.reccords.length
      }else {
        // this.beep(200)
      }
    },
    updateTime:function(){
      let d = new Date();
      let t = d.getTime();
      this.time = t
    },
    stopAll: function(){
      clearTimeout(this.timer)
      this.elements = this.pattern = this.reccords = this.diffs = []
      this.timer = ""
      this.playMode = this.nextPulseFlag = false
      this.boxes.matrix = this.estimTBP = this.time = 0
      console.log(this.reccords);
    },
    beep:function(note){
      var context = new AudioContext()
      var o = context.createOscillator()
      var  g = context.createGain()
      o.frequency.value = note
      o.connect(g)
      g.connect(context.destination)
      o.start(0)
      g.gain.value = .05
      g.gain.exponentialRampToValueAtTime(0.00001, context.currentTime + 0.05)
    }
  },
  props: {
  },
})
