// usage $ node hit.js 13
// where 13 is the raspberry pi pin number

let Gpio = require('onoff').Gpio; //include onoff to interact with the GPIO
let pin = process.argv[2]

if(pin == undefined){
  console.log('pin argument missing')
}else{
  let LED = new Gpio(pin, 'out');
  let blinkInterval = setInterval(blinkLED, 50);

  function blinkLED() {
    if (LED.readSync() === 0) {
      LED.writeSync(1); //set pin state to 1 (turn LED on)
      console.log('1')
    } else {
      LED.writeSync(0); //set pin state to 0 (turn LED off)
      console.log('0')
    }
  }

  function endBlink() { //function to stop blinking
    clearInterval(blinkInterval); // Stop blink intervals
    LED.writeSync(0); // Turn LED off
    LED.unexport(); // Unexport GPIO to free resources
  }
  setTimeout(endBlink, 50000); //stop blinking after 5 seconds
}
