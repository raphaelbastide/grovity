require('./check-versions')()
var config = require('../config')
if (!process.env.NODE_ENV) process.env.NODE_ENV = JSON.parse(config.dev.env.NODE_ENV)
var path = require('path')
var ip = require("ip");
var express = require('express')
var webpack = require('webpack')
var opn = require('opn')
var proxyMiddleware = require('http-proxy-middleware')
var webpackConfig = require('./webpack.dev.conf')
if (config.dev.hasGPIO) {
  var Gpio = require('onoff').Gpio; //include onoff to interact with the GPIO
}
if (config.dev.hasGPIO) {
  var pushButton = new Gpio(17, 'in', 'both');
}
var port = process.env.PORT || config.dev.port
var proxyTable = config.dev.proxyTable

var app = express()
var compiler = webpack(webpackConfig)

var devMiddleware = require('webpack-dev-middleware')(compiler, {
  publicPath: webpackConfig.output.publicPath,
  stats: {
    colors: true,
    chunks: false
  }
})

var hotMiddleware = require('webpack-hot-middleware')(compiler)
compiler.plugin('compilation', function (compilation) {
  compilation.plugin('html-webpack-plugin-after-emit', function (data, cb) {
    hotMiddleware.publish({ action: 'reload' })
    cb()
  })
})
Object.keys(proxyTable).forEach(function (context) {
  var options = proxyTable[context]
  if (typeof options === 'string') {
    options = { target: options }
  }
  app.use(proxyMiddleware(context, options))
})

app.use(require('connect-history-api-fallback')())
app.use(devMiddleware)
app.use(hotMiddleware)
var staticPath = path.posix.join(config.dev.assetsPublicPath, config.dev.assetsSubDirectory)
app.use(staticPath, express.static('./static'))

const server = app.listen(port, function (err) {
  if (err) {
    console.log(err)
    return
  }
  console.log('Local URI: ' + 'http://localhost:' + port )
  console.log('Remote URI: http://'+ip.address()+':'+port+'\n' );
  // opn(uri)
})

var io = require('socket.io')(server);

function hitSolenoid(pin) {
  pin.writeSync(1)
  setTimeout(function(){pin.writeSync(0)},50)
}

io.on('connection', function(socket) {
  if (config.dev.hasGPIO) {
    pushButton.watch(function (err, data) { //Watch for hardware interrupts on pushButton
      if (err) { //if an error
        console.error('There was an error', err); //output error message to console
        return;
      }
      val = data;
      socket.emit('hit', 'manual', 4); //send button status to client
    });
  }

  console.log(socket.id)
  socket.on('play', function() {
    console.log('play')
  });
  socket.on('stop', function() {
    console.log('stop')
  });
  socket.on('tick', function() {
    socket.emit('tick');
  });
  socket.on('hit', function(type, solenoid) {
    console.log('hit', type, solenoid)
    if (config.dev.hasGPIO) {
      // if (solenoid != LED.readSync()) { // check if sol is in use
        var pin = new Gpio(solenoid, 'out'); //use GPIO pin 4 as output
        hitSolenoid(pin)
      // }
    }
  });
});
