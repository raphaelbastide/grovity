import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    title:"untitled",
    comment:"",
    theme:"dark",
    instruments: [],
    pinList:[4, 8, 16, 18],
    isPlaying:false,
    isMute:false,
    isInactive:false,
    isWebaudio:true,
    isMidi:true,
    isGPIO:false,
    midiOutput:"",
    saveIndicator:false,
    bpm:100,
    interPadTime:0,
    time: -1,
    showComment: false
  },
  mutations: {
    loadJSON(state, json) {
      state.instruments = JSON.parse(json);
    },
    showSaveIndicator(){
      let that = this.state
      that.saveIndicator = true
      setTimeout(function () {
        that.saveIndicator = false
      }, 200);
    },
    // TODO
    // saveState() {
    //   localStorage.setItem('grovity', this.getters.toJSON);
    //   this.showSaveIndicator
    // },
    initMidi(state, WebMidi){
      if (WebMidi.enabled) {
        console.log('Midi already enabled');
        state.midiOutput = WebMidi.outputs[0]
      }else {
        WebMidi.enable(function (err) {
          if (err) {
            console.log(err);
            state.isMidi = false
            return
          }else{
            console.log("WebMidi enabled!");
            state.isMidi = true
            let midiInputs = WebMidi.inputs
            let midiOutputs = WebMidi.outputs
            console.log("Midi inputs:");
            for (var i = 0; i < midiInputs.length; i++) {
              console.log(i+" – "+midiInputs[i].name);
            }
            console.log("Midi outputs:");
            for (var i = 0; i < midiOutputs.length; i++) {
              console.log(i+" – "+midiOutputs[i].name);
            }
            console.log("Chosen output: "+WebMidi.outputs[0].name);
            console.log(state.midiOutput);
          }
          console.log(WebMidi.outputs[0]);
          state.midiOutput = WebMidi.outputs[0]
        }, true);
      }
    },
  },
  getters:{
    doneInstruments(state) {
      return state.instruments.filter(instrument => instrument.done);
    },
    allInstruments(state) {
      return state.instruments;
    },
    bpm(state) {
      return state.bpm;
    },
    title(state) {
      return state.title;
    },
    comment(state) {
      return state.comment;
    },
    updatePadNbr(state, instrument) {
      return state.instruments.filter(instrument => instrument.pads);
    },
    toJSON(state) {
      return JSON.stringify(state.instruments);
    },
  },
})

export default store
